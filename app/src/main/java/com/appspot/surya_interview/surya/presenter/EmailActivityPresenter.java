package com.appspot.surya_interview.surya.presenter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;

import com.appspot.surya_interview.surya.api.ApiCallback;
import com.appspot.surya_interview.surya.api.RetrofitInterface;
import com.appspot.surya_interview.surya.model.Items;
import com.appspot.surya_interview.surya.model.ListRequest;
import com.appspot.surya_interview.surya.model.ListResponse;
import com.appspot.surya_interview.surya.utils.AppAlerts;
import com.appspot.surya_interview.surya.utils.CheckNetworkConnection;
import com.appspot.surya_interview.surya.utils.SharedPrefUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class EmailActivityPresenter {


    private View view;
    Activity context;

    public EmailActivityPresenter(View view, Activity context) {
        this.context=context;
        this.view = view;
    }

    public void updateEmail(String email){
      //  user.setEmail(email);
        if (isValidEmail(email)){
            view.EnableSubmitButton(true);
        }
    }

    private static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public void getListItems(String email) {
        ListRequest listRequest=new ListRequest();
        listRequest.setEmailId(email);
        createRequestforIteams(listRequest);
    }

    public interface View{

        void updateUserInfoTextView(String info);
        void EnableSubmitButton( boolean b);

     void showProgressBar();
     void hideProgressBar();
     void showUserList(List<Items> items);

    }



    private void createRequestforIteams(ListRequest listRequest){
        view.showProgressBar();
        if (CheckNetworkConnection.isOnline(context)) {

            Call<ListResponse> callRegisterUser = RetrofitInterface.getRestApiMethods(context).getItemList(listRequest);
            callRegisterUser.enqueue(new ApiCallback<ListResponse>(context) {
                @Override
                public void onApiResponse(Response<ListResponse> response, boolean isSuccess, String message) {
                    ListResponse listResponse = response.body();
                    if (isSuccess) {
                        view.hideProgressBar();
                        if (listResponse!=null && listResponse.getItems()!=null && listResponse.getItems().size()>0){
                            SharedPrefUtils.setUsersList(context,listResponse.getItems());
                            view.showUserList(listResponse.getItems());
                        }
                    } else {
                        view.hideProgressBar();
                    }
                }

                @Override
                public void onApiFailure(boolean isSuccess, String message) {
                    AppAlerts.showToast(context, "Error: "+message);
                   // AppAlerts.showToast(context, "Error, Please try after sometime");
                    view.hideProgressBar();
                }
            });
        } else {
            AppAlerts.showNetworkAlert(context);
            view.hideProgressBar();
        }

    }

}
