package com.appspot.surya_interview.surya.view;

import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.appspot.surya_interview.surya.R;

public class SplashScreen extends BaseActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    TextView tvAppVersion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        tvAppVersion=findViewById(R.id.tv_AppVersion);

        tvAppVersion.setText("version: "+ getVersionInfo(this));

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app EmailActivity activity, with forward animation.

                frwdAnimIntent(SplashScreen.this, EmailActivity.class);
            // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }



}
