package com.appspot.surya_interview.surya.utils;

import android.content.Context;
import android.content.SharedPreferences;


import com.appspot.surya_interview.surya.model.Items;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class SharedPrefUtils {


    public static String PREF_NAME = "SuryaPref";
    private static String PREF_USER = "items";

    Context context;


    public static SharedPreferences getSharedPref(Context context) {
        if (context != null) {
            if (context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE) != null) {
                return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
            } else {
                return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
            }
        } else {
            return null;
        }
    }

    public static void putData(Context context, String key, String value) {
        if (context != null) {
            if (getSharedPref(context) != null) {
                getSharedPref(context).edit().putString(key, value).apply();

            }
        }
    }

    public static void putData(Context context, String key, int value) {
        if (context != null) {
            if (getSharedPref(context) != null) {
                getSharedPref(context).edit().putInt(key, value).apply();

            }
        }
    }

    public static void putData(Context context, String key, boolean value) {
        if (context != null) {
            if (getSharedPref(context) != null) {
                getSharedPref(context).edit().putBoolean(key, value).apply();

            }
        }
    }

    public static String getString(Context context, String key, String defaultValue) {
        if (context != null) {
            if (getSharedPref(context) != null) {
                return getSharedPref(context).getString(key, defaultValue);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public static boolean getBoolean(Context context, String key, boolean defaultValue) {
        if (context != null) {
            if (getSharedPref(context) != null) {
                return getSharedPref(context).getBoolean(key, defaultValue);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static int getInt(Context context, String key, int defaultValue) {
        if (context != null) {
            if (getSharedPref(context) != null) {
                return getSharedPref(context).getInt(key, defaultValue);
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public void clearSharePref() {
        SharedPreferences preferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    //========= Get and set of UsersList =============

    public static void setUsersList(Context context, List<Items> items) {
        if (context == null)
            return;
        String json = new Gson().toJson(items);
        putData(context, PREF_USER, json);   // If we need to get sore the data based on  emailid change the  PREF_USER to EmailId
    }



    public static ArrayList<Items> getUsersList(Context context, String EmailId) {
        if (context == null && PREF_USER == null) {  // If we need to get sore the data based on  emailid change the  PREF_USER to EmailId
            return null;
        } else {
            ArrayList<Items> serchList = null;
            String userStr = getString(context, PREF_USER, null);
            if (userStr != null) {
                Gson gson = new Gson();
                serchList = gson.fromJson(userStr, new TypeToken<ArrayList<Items>>() {
                }.getType());
            }
            return serchList;
        }
    }

    //=================================================
}
