package com.appspot.surya_interview.surya.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.appspot.surya_interview.surya.R;

public class AppAlerts {


    public static final int BUTTON_POSITIVE = 0;
    public static final int BUTTON_NEGATIVE = 1;
    private static AlertDialog alertDialog;


    public static void showAlert(Activity activity, String message, String positiveButton, String negativeButton) {
        if (!activity.isFinishing()) {
            showAlert(activity, null, message, positiveButton, negativeButton, null);
        }
    }

    public static void showAlert(Activity activity, String message, String positiveButton, String negativeButton, final AlertListener listener) {
        if (!activity.isFinishing()) {
            showAlert(activity, null, message, positiveButton, negativeButton, listener);
        }
    }

    public static void showAlert(Activity activity, String message, String positiveButton, final AlertListener listener) {
        if (!activity.isFinishing()) {
            showAlert(activity, null, message, positiveButton, null, listener);
        }
    }

    public static void showAlert(Activity activity, String message, String positiveButton) {
        if (!activity.isFinishing()) {
            showAlert(activity, null, message, positiveButton, null, null);
        }
    }

    /**
     * @param activity
     * @param message
     * @param positiveButton -
     * @param negativeButton
     * @param listener
     */
    public static void showAlert(Activity activity, String title, String message, String positiveButton, String negativeButton, final AlertListener listener) {
         try {
            if (alertDialog != null)
                alertDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        View inflate = activity.getLayoutInflater().inflate(R.layout.inflate_alert, null);
        builder.setView(inflate);
        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        TextView messageView = inflate.findViewById(R.id.alert_message);
        TextView titleView = inflate.findViewById(R.id.alert_title);
        if (title != null)
            titleView.setText(title);

        messageView.setText(message);

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int whichBtn = -1;
                switch (view.getId()) {
                    case R.id.alert_negative:
                        whichBtn = BUTTON_NEGATIVE;
                        break;
                    case R.id.alert_positive:
                        whichBtn = BUTTON_POSITIVE;
                        break;
                }
                if (listener != null) {
                    listener.onClick(whichBtn);
                }
                alertDialog.dismiss();


            }
        };
        if (positiveButton != null) {
            Button positiveButtonView = inflate.findViewById(R.id.alert_positive);
            positiveButtonView.setText(positiveButton);
            positiveButtonView.setOnClickListener(clickListener);
            positiveButtonView.setVisibility(View.VISIBLE);
        }

        if (negativeButton != null) {
            Button negativeButtonView = inflate.findViewById(R.id.alert_negative);
            negativeButtonView.setText(negativeButton);
            negativeButtonView.setOnClickListener(clickListener);
            negativeButtonView.setVisibility(View.VISIBLE);
        }


        alertDialog.show();
    }



    public static void showToast(Context context, String message) {
        try {
            if (context == null) return;
            if (message == null || !message.isEmpty()) {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void showNetworkAlert(Context context) {
        try {
            AppAlerts.showToast(context, "Please check the internet connection");
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void dismiss() {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }
    public interface AlertListener {
        void onClick(int buttonType);
    }


}
