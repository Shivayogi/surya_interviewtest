package com.appspot.surya_interview.surya.view;

import android.Manifest;
import android.app.Activity;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.appspot.surya_interview.surya.R;
import com.appspot.surya_interview.surya.adapters.UserListAdapter;
import com.appspot.surya_interview.surya.model.Items;
import com.appspot.surya_interview.surya.presenter.EmailActivityPresenter;
import com.appspot.surya_interview.surya.utils.SharedPrefUtils;
import com.appspot.surya_interview.surya.utils.SpacesItemDecoration;
import com.appspot.surya_interview.surya.utils.Utils;

import java.util.List;

public class EmailActivity extends BaseActivity implements EmailActivityPresenter.View, AdapterView.OnItemClickListener {


    int PERMISSION_ALL = 1;
    private EmailActivityPresenter presenter;
    private ProgressBar progressBar;
    Button btnSubmit;
    EditText email;
    LinearLayout lyEamil,lyList;
    RecyclerView rvUserList;
    UserListAdapter UserListAdapter;
    List<Items> items;


    String[] PERMISSIONS = {Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        presenter=new EmailActivityPresenter(this,this);
        email = findViewById(R.id.email);
        btnSubmit = findViewById(R.id.btn_Submit);

        lyEamil = findViewById(R.id.ly_Email);
        lyList = findViewById(R.id.ly_List);
        rvUserList = findViewById(R.id.rv_UserList);



        try {
            String emailId = SharedPrefUtils.getString(EmailActivity.this,"EmailId","");
            if (!Utils.isStringEmpty(emailId)){
                items =  SharedPrefUtils.getUsersList(this,emailId);
                if (items!=null && items.size()>0){
                    initList(items);
                    presenter.getListItems(emailId);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                presenter.updateEmail(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                try {
                    if (email!=null && !isStringEmpty(email.getText().toString().trim()) ){
                        SharedPrefUtils.putData(EmailActivity.this,"EmailId",email.getText().toString().trim());
                        presenter.getListItems(email.getText().toString().trim());
                    }else{
                        email.setError("Please enter email id");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void initList(List<Items> items) {
        try {

            lyEamil.setVisibility(View.GONE);
            lyList.setVisibility(View.VISIBLE);

            UserListAdapter = new UserListAdapter(this, items, this, this);
            rvUserList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            rvUserList.setAdapter(UserListAdapter);
       /*     SpacesItemDecoration spacesItemDecoration1 = new SpacesItemDecoration(10, DividerItemDecoration.VERTICAL);
            rvUserList.addItemDecoration(spacesItemDecoration1);*/
            rvUserList.addItemDecoration(
                    new DividerItemDecoration(EmailActivity.this,  DividerItemDecoration.VERTICAL));
            UserListAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateUserInfoTextView(String info) {

    }

    @Override
    public void EnableSubmitButton(boolean b) {
        btnSubmit.setEnabled(b);
    }

    @Override
    public void showProgressBar() {
        showProgress();
    }

    @Override
    public void hideProgressBar() {
        dismissProgress();
    }

    @Override
    public void showUserList(List<Items> items) {
        initList(items);
    }

//==========================UserListAdapter OnItemClick Listener =========================================
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (view==null){
            return;
        }
    }
 //=================================================================================================



}
