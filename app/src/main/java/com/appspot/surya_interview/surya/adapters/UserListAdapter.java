package com.appspot.surya_interview.surya.adapters;

import android.content.ClipData;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appspot.surya_interview.surya.R;
import com.appspot.surya_interview.surya.model.Items;
import com.appspot.surya_interview.surya.model.ListResponse;
import com.appspot.surya_interview.surya.utils.ColorGenerator;
import com.appspot.surya_interview.surya.utils.TextDrawable;
import com.appspot.surya_interview.surya.utils.Utils;
import com.appspot.surya_interview.surya.view.EmailActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder> {

    private Context mContext;
    private static AdapterView.OnItemClickListener onItemClickListener;

    public ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;
    public TextDrawable.IBuilder mDrawableBuilder;

    List<Items> items;

    public UserListAdapter(Context context, List<Items> items, AdapterView.OnItemClickListener onItemClickListener, EmailActivity emailActivity) {
        this.items = items;
        this.mContext = context;
        UserListAdapter.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public UserListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_userlist, null);

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull UserListAdapter.ViewHolder viewHolder, int i) {

        if (items != null && items.size() > 0 && items.get(i) != null) {
            Items mItems = items.get(i);
            String name = "";
            if (!Utils.isStringEmpty(mItems.getFirstName())) {
                if (!Utils.isStringEmpty(mItems.getLastName())) {

                    name = name + mItems.getFirstName() + " " + mItems.getLastName();
                } else {
                    name = name + mItems.getFirstName();
                }
                viewHolder.tvUserFname.setText(name);
            }

            if (!Utils.isStringEmpty(mItems.getEmailId())) {
                viewHolder.tvEmailId.setText(mItems.getEmailId());
            }

            if (!Utils.isStringEmpty(mItems.getImageUrl())) {
                Glide.with(mContext)
                        .load(mItems.getImageUrl())
                        .override(100, 100)
                        .fitCenter()
                        .error(R.drawable.ic_placeholder)
                        .placeholder(R.drawable.ic_placeholder)
                        .into(viewHolder.imgUser);


            } else {
                int color = mColorGenerator.getColor(mItems.getFirstName());
                TextDrawable drawable = mDrawableBuilder.build(String.valueOf(mItems.getFirstName().substring(0, 1)), color);
                viewHolder.imgUser.setImageDrawable(drawable);
            }
        }
    }


    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvUserFname, tvUserLname, tvEmailId;
        ImageView imgUser;

        ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            tvUserFname = itemLayoutView.findViewById(R.id.img_UserFName);
            // tvUserLname = itemLayoutView.findViewById(R.id.img_UserLName);
            tvEmailId = itemLayoutView.findViewById(R.id.img_UserEmail);
            imgUser = itemLayoutView.findViewById(R.id.img_User);

        }

        @Override
        public void onClick(View view) {
            onItemClickListener.onItemClick(null, view, getAdapterPosition(), view.getId());
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }

}
