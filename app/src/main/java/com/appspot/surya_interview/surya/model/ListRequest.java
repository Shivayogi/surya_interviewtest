package com.appspot.surya_interview.surya.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListRequest {

    @SerializedName("emailId")
    @Expose
    private String emailId;

    public ListRequest() {
    }


    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }


    @Override
    public String toString() {
        return "ListRequest{" +
                "emailId='" + emailId + '\'' +
                '}';
    }


}
