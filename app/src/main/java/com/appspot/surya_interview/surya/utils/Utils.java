package com.appspot.surya_interview.surya.utils;

public class Utils {

    public static boolean isStringEmpty(String input) {
        Boolean valid = false;
        if (input == null) {
            valid = true;
        } else if (input.trim().length() == 0) {
            valid = true;
        }
        return valid;
    }
}
