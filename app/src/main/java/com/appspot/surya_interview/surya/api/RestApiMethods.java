package com.appspot.surya_interview.surya.api;


import com.appspot.surya_interview.surya.model.ListRequest;
import com.appspot.surya_interview.surya.model.ListResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;



public interface RestApiMethods {

    String CACHE_CONTROL = "cache-control: no-cache";
    String BASE_URL ="http://surya-interview.appspot.com/";
    String CONTENT_TYPE = "content-type:application/json";


    @POST(BASE_URL+"list")
    @Headers(CONTENT_TYPE)
    Call<ListResponse> getItemList(@Body ListRequest emailId);

}
